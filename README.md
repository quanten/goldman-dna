# Goldman DNA

Implementation of the algorithm to convert files into DNA strands of 
[Goldman, et al. (2013)](https://www.nature.com/articles/nature11875/), 
[version 2.0 from 21. August 2013](https://www.ebi.ac.uk/sites/ebi.ac.uk/files/groups/goldman/file2features_2.0.pdf).

## Usage

```
$ python3 -m goldman_dna --help
usage: __main__.py [-h] [-v] [-l] [-c] (-e | -d) [-m MULTI_FILE | -i ID] file [file ...]

A implementation of the method from Goldman et. al (2013) to encode arbitrary files into DNA fragments.

positional arguments:
  file                  the file to encode/decode

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         print more information
  -l, --long-strand     don't split strand, only perform step 2-4. many options won't work in this mode
  -c, --compression     use compression (for encoding / decoding)
  -e, --encode          encode file
  -d, --decode          decode file
  -m MULTI_FILE, --multi-file MULTI_FILE
                        prefix for output file. encodes up to 9 files into one DNA file, distinguished by file id / decode all files in DNA file
  -i ID, --id ID        encode with this file id / decode file with this file id, default 0
```

Use `-e` for encoding files into DNA and `-d` to decode DNA into files. 

With `-e -m <prefix>` up to nine files will be encoded into one DNA file named `<prefix>.dna[.comp]`. 
To decode them, use `-d -m <prefix>` to decode all files, which will be named `prefix.(0..9)`, found in the DNA.

When not using `-m`, all files will be handled separated. Then `-i` can be used to specify the file id which should be
used for encoding and decoding.

With `-c`, the DNA input or output will be compressed, otherwise the nucleotides are saved as ASCII chars A, T, G and C.

The long strand mode `-l` only executes Steps 1.2 to 1.4 of the algorithm and generates a single long DNA strand.
Other options like `-m` or `-c` are not available in this mode! Also note, that this will not work with the [original 
files from the paper](https://www.ebi.ac.uk/goldman-srv/DNA-storage/orig_files/DNA_versions/), 
since they were generated with version 1.0 of the method, which differs in the length information encoding.

Use `-v` for a verbose output.

## Documentation

All function interfaces are documented through docstrings.
View them directly in the source or with pydoc (`-b` for browser):

```shell script
pydoc [-b] goldman_dna
```

## Distribution

To package the project, use the makefile:

```shell script
make build
```

## Development

To run the tests and linter, use the makefile:

```shell script
make test
```
