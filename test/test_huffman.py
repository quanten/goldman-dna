#!/bin/env python3

import unittest

from goldman_dna import huffman


class HuffmanTest(unittest.TestCase):

    def test_completeness_of_conversion(self):
        self.assertEqual(257, len(huffman.BYTE_TO_TRITS))
        self.assertEqual(257, len(huffman.TRITS_TO_BYTE))
        for i in range(256):
            self.assertTrue(i in huffman.BYTE_TO_TRITS)

    def test_reversion_of_conversion_dicts(self):
        self.assertEqual(huffman.BYTE_TO_TRITS,
                         {v: k for k, v in
                          huffman.TRITS_TO_BYTE.items()})
        self.assertEqual(huffman.TRITS_TO_BYTE,
                         {v: k for k, v in
                          huffman.BYTE_TO_TRITS.items()})

    def test_reversion_of_data(self):
        self.assertEqual(b'', huffman.decode(huffman.encode(b'')))
        self.assertEqual(b'testing',
                         huffman.decode(huffman.encode(b'testing')))
        test_bytes = bytearray()
        for i in range(256):
            test_bytes.append(i)
        self.assertEqual(test_bytes,
                         huffman.decode(huffman.encode(test_bytes)))


if __name__ == '__main__':
    unittest.main()
