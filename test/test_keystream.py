#!/bin/env python3

import unittest

from goldman_dna import keystream


class KeystreamTest(unittest.TestCase):
    test_strings = ["AGTC" * 25, "AT" * 50, "GC" * 50, "AC" * 50, "GT" * 50]

    def test_preservation_of_length(self):
        for string in self.test_strings:
            for i in range(4):
                self.assertEqual(100, len(keystream.encrypt(string, i)))
                self.assertEqual(100, len(keystream.decrypt(string, i)))

    def test_reversion(self):
        for string in self.test_strings:
            for i in range(4):
                self.assertEqual(string,
                                 keystream.decrypt(keystream.encrypt(string, i),
                                                   i))


if __name__ == '__main__':
    unittest.main()
