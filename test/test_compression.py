#!/bin/env python3

import unittest

from goldman_dna import compression


class CompressionTest(unittest.TestCase):
    def test_reversion(self):
        test_strings = [
            "TGCA" * 117, "TA" * 117, "CAT" * 117, "CTAG" * 117 * 25
        ]
        for string in test_strings:
            self.assertEqual(string, compression.decompress(
                compression.compress(string)))


if __name__ == '__main__':
    unittest.main()
