#!/bin/env python3

import unittest

import goldman_dna
from goldman_dna import utils


class HelperFunctionsTest(unittest.TestCase):
    def test_int_to_base3(self):
        self.assertEqual("0", utils.int_to_base3_str(0))
        self.assertEqual("1", utils.int_to_base3_str(1))
        self.assertEqual("2", utils.int_to_base3_str(2))
        self.assertEqual("212121", utils.int_to_base3_str(637))

    def test_base3_to_int(self):
        self.assertEqual(0, utils.base3_str_to_int(""))
        self.assertEqual(0, utils.base3_str_to_int("0"))
        self.assertEqual(1, utils.base3_str_to_int("1"))
        self.assertEqual(2, utils.base3_str_to_int("2"))
        self.assertEqual(637, utils.base3_str_to_int("212121"))

    def test_reversion_of_base3_conversion(self):
        test_numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 42, 1337, 2 ** 10,
                        2 ** 15]
        for number in test_numbers:
            self.assertEqual(number, utils.base3_str_to_int(
                utils.int_to_base3_str(number)))

    def test_complement(self):
        self.assertEqual("ATGC", utils.complement_nucleotides("TACG"))

    def test_checksum(self):
        self.assertEqual("0", utils.compute_checksum(
            utils.fragement_id_to_i_3(6), "12"))
        self.assertEqual("2", utils.compute_checksum(
            utils.fragement_id_to_i_3(6), "00"))

        # example from paper
        self.assertEqual("1", utils.compute_checksum(
            utils.fragement_id_to_i_3(0), "12"))
        self.assertEqual("1", utils.compute_checksum(
            utils.fragement_id_to_i_3(1), "12"))

    def test_duplicate_removal(self):
        self.assertEqual("ATA", goldman_dna._remove_duplicates("ATA"))
        self.assertEqual("ATA", goldman_dna._remove_duplicates("ATTTAAAA"))
        self.assertEqual("ATAGA", goldman_dna._remove_duplicates("ATTTAAGAA"))
        self.assertEqual("ATA", goldman_dna._remove_duplicates("AATTTAAAA"))


if __name__ == '__main__':
    unittest.main()
