#!/bin/env python3

import unittest

import goldman_dna
from goldman_dna import steps
from goldman_dna import utils


class ExampleFromPaperTest(unittest.TestCase):

    example_data_from_paper = "Birney and Goldman"
    example_result_from_paper = [
            "ACGTGTACGACTCGACAGAGATGCAGAGACTAGACACTCTCTATACGCATCTCAGACACTCGTAGT"
            "CTACTGACATCGCACGAGAGAGAGCAGCGCACTCAGCGTACGTACGTACTG",
            "TATATATAGCGTGATAGCGTCAGTCGCATCGCACACGAGCTCTCTCTAGCATAGCGTCTAGATGCG"
            "ACGAGCGAGCTCATGTGATCTGCGTACTCTCTACAGCGTACGTACGTAGAC"
        ]
    watson_crick = "We wish to suggest a structure for the salt of deoxyribose nucleic acid (D.N.A). This structure has novel features which are considerable biological interest."

    def test_steps_with_example(self):
        """
        Tests with the example given in https://www.ebi.ac.uk/sites/ebi.ac.uk/files/groups/goldman/file2features_2.0.pdf
        """
        ID = 5  # 12 in base3
        s_0 = "Birney and Goldman".encode("utf-8")
        s_1_example = "20100202101010100021200012221110221201112000212210002" \
                      "212222212021100210122100110210111200021"
        s_4_example = "00000000000000000000101022010020210101010002120001222" \
                      "11102212011120002122100022122222120211002101221001102" \
                      "1011120002100000000"
        s_5_example = "CGTACGTACGTACGTACGTAGTCGCACTACACAGTCGACTACGCTGTACTGCA" \
                      "GAGTGCTGTCTCACGTGATGACGTGCTGCATGATATCTACAGTCATCGTCTAT" \
                      "CGAGATACGCTACGTACGT"
        f_i_example = ["CGTACGTACGTACGTACGTAGTCGCACTACACAGTCGACTACGCTGTACTGCAGA"
                       "GTGCTGTCTCACGTGATGACGTGCTGCATGATATCTACAGTCATC",
                       "ACGTACGTAGCGTATCTCGATAGACGATGACTGTAGATATCATGCAGCACGTCAT"
                       "CACGTGAGACAGCACTCTGCAGTACAGCGTAGTCGACTGTGTAGT"]
        f_i_keyed_example = [
            "CGTGTACGACTCGACAGAGATGCAGAGACTAGACACTCTCTATACGCATCTCAGACACTCGTAGTC"
            "TACTGACATCGCACGAGAGAGAGCAGCGCACTCA",
            "ATATATAGCGTGATAGCGTCAGTCGCATCGCACACGAGCTCTCTCTAGCATAGCGTCTAGATGCGA"
            "CGAGCGAGCTCATGTGATCTGCGTACTCTCTACA"]
        f_i_indexed_example = [
            "CGTGTACGACTCGACAGAGATGCAGAGACTAGACACTCTCTATACGCATCTCAGACACTCGTAGTC"
            "TACTGACATCGCACGAGAGAGAGCAGCGCACTCAGCGTACGTACGTACT",
            "ATATATAGCGTGATAGCGTCAGTCGCATCGCACACGAGCTCTCTCTAGCATAGCGTCTAGATGCGA"
            "CGAGCGAGCTCATGTGATCTGCGTACTCTCTACAGCGTACGTACGTAGA"
        ]
        f_i_finished_example = self.example_result_from_paper
        s_1 = steps.step_2(s_0)
        self.assertEqual(s_1_example, s_1)
        s_4 = steps.step_3(s_1)
        self.assertEqual(s_4_example, s_4)
        s_5 = steps.step_4(s_4)
        self.assertEqual(s_5_example, s_5)
        f_i = steps.step_5_6(s_5)
        self.assertListEqual(f_i_example, f_i)
        f_i_keyed = list(
            map(lambda x: steps.step_7(f_i[x], x), range(len(f_i))))
        self.assertListEqual(f_i_keyed_example, f_i_keyed)
        f_i_indexed = []
        for i in range(len(f_i_keyed)):
            i_3, file_id, parity = steps.step_8(i, ID)
            f_i_indexed.append(steps.step_9(f_i_keyed[i], i_3, file_id, parity))
        self.assertListEqual(f_i_indexed_example, f_i_indexed)
        f_i_finished = []
        for i in range(len(f_i_indexed)):
            # the i * 3 assures the same choice of nucleotides as in the example
            f_i_finished.append(steps.step_10(f_i_indexed[i], i * 3))
        self.assertListEqual(f_i_finished_example, f_i_finished)

    def test_example(self):
        f_i = goldman_dna.encode(self.example_data_from_paper.encode("utf-8"),
                                 file_id_num=5,
                                 random_numbers= [0,3].__iter__().__next__)
        self.assertListEqual(self.example_result_from_paper, f_i)
        self.assertEqual(self.example_data_from_paper,
                         goldman_dna.decode(f_i)[5].decode("utf-8"))

    def test_error_correction(self):
        raw_data = self.watson_crick.encode("utf-8")
        f_i = goldman_dna.encode(raw_data, file_id_num=2, random_numbers=range(50).__iter__().__next__)
        self.assertEqual(raw_data, goldman_dna.decode(f_i)[2])
        # test missing strings
        del f_i[5]
        del f_i[5]
        del f_i[5]
        self.assertEqual(raw_data, goldman_dna.decode(f_i)[2])
        tmp = f_i[5] # four missing strings are not recoverable
        del f_i[5]
        self.assertEqual(None, goldman_dna.decode(f_i)[2])
        f_i.append(tmp)
        # therefore, f_i[-1] is now vital
        self.assertEqual(raw_data, goldman_dna.decode(f_i)[2])
        # test reverse complement
        f_i[-1] = utils.complement_nucleotides(f_i[-1])[::-1]
        self.assertEqual(raw_data, goldman_dna.decode(f_i)[2])
        # test duplicated nucleotide
        f_i[-1] = f_i[-1][:7] + f_i[-1][6:]
        f_i[-1] = f_i[-1][:15] + f_i[-1][15:]
        self.assertEqual(raw_data, goldman_dna.decode(f_i)[2])
        # test shuffle
        permutations = [(1,2), (4,9), (5,8), (3,12), (20, 7)]
        for perm in permutations:
            tmp = f_i[perm[0]]
            f_i[perm[0]] = f_i[perm[1]]
            f_i[perm[1]] = tmp
        self.assertEqual(raw_data, goldman_dna.decode(f_i)[2])



if __name__ == '__main__':
    unittest.main()
