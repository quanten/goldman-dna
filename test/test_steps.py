#!/bin/env python3

import unittest

from goldman_dna import steps, utils


class Step13456Test(unittest.TestCase):
    test_strings = ["", "0", "1", "2", "012" * 9, "201" * 10, "201" * 33]
    test_strings_nucleotides = [("T" * 25 + "A" * 25 + "G" * 25) * 5,
                                "AT" * 100]
    test_strings_fragments = ["AGTC" * 25, "AT" * 50, "GC" * 50, "AC" * 50,
                              "GT" * 50]

    # step 2 is tested in test_huffman

    def test_length_multiple_25_of_step_3(self):
        self.assertEqual(0, len(steps.step_3("")) % 25)
        self.assertEqual(0, len(steps.step_3("1")) % 25)
        self.assertEqual(0, len(steps.step_3("2")) % 25)
        self.assertEqual(0, len(steps.step_3("12")) % 25)
        self.assertEqual(0, len(steps.step_3("1212121212")) % 25)
        self.assertEqual(0, len(steps.step_3("201" * 9)) % 25)

    def test_reversion_of_step_3(self):
        for string in self.test_strings:
            self.assertEqual(string,
                             steps.reverse_step_3(steps.step_3(string)))

    def test_preservation_of_length_of_step_4(self):
        for string in self.test_strings:
            self.assertEqual(len(string), len(steps.step_4(string)))

    def test_reversion_of_step_4(self):
        for string in self.test_strings:
            self.assertEqual(string,
                             steps.reverse_step_4(steps.step_4(string)))

    def test_length_of_step_5_6(self):
        f_i = steps.step_5_6("ATG" * 100)
        self.assertEqual(9, len(f_i))
        for f in f_i:
            self.assertEqual(100, len(f))

    def test_reversion_of_step_5_6(self):
        for string in self.test_strings_nucleotides:
            self.assertEqual(string,
                             steps.reverse_step_5_6(steps.step_5_6(string)))

    def test_exception_in_reversion_step_5_6(self):
        string = "ATG" * 100
        f_i = steps.step_5_6(string)
        self.assertEqual(string, steps.reverse_step_5_6(f_i))
        f_i[1] = "C" + f_i[1][1:]
        self.assertRaises(ValueError, steps.reverse_step_5_6, f_i)

    def test_reassemble_step_5_6(self):
        string = "ATG" * 250
        f_i = steps.step_5_6(string)
        self.assertEqual(string, steps.reassemble_step_5_6(dict(enumerate(f_i))))
        f_i[1] = "C" + f_i[1][1:]
        self.assertEqual(string, steps.reassemble_step_5_6(dict(enumerate(f_i))))

    # step 7 is tested in test_keystream

    def test_length_of_step_8(self):
        for i in [0, 1, 5, 42, 1337]:
            for file_id in range(9):
                self.assertTupleEqual((12, 2, 1),
                                      tuple(map(len, steps.step_8(i, file_id))))

    def test_reversion_of_step_8(self):
        for i in [0, 1, 5, 42, 1337]:
            for file_id_num in range(9):
                i_3, file_id, parity = steps.step_8(i, file_id_num)
                r_index, r_file_id = steps.reverse_step_8(i_3, file_id, parity)
                self.assertEqual(i, r_index)
                self.assertEqual(file_id_num, r_file_id)

    def test_parity_of_reverse_step_8(self):
        for i in [0, 1, 5, 42, 1337]:
            for file_id in range(9):
                i_3, file_id, parity = steps.step_8(i, file_id)
                wrong_parity = (int(parity) + 1 ) % 3
                self.assertRaises(ValueError, steps.reverse_step_8, i_3, file_id, wrong_parity)

    def test_length_of_step_9(self):
        string = "ACTG" * 25
        for i in [0, 1, 5, 42, 1337]:
            for file_id in range(9):
                self.assertEqual(115,
                                 len(steps.step_9(string,
                                                        utils.fragement_id_to_i_3(
                                                              i),
                                                          f"{utils.int_to_base3_str(file_id):0>2}",
                                                          "0")))

    def test_reversion_of_step_9(self):
        for string in self.test_strings_fragments:
            for i in [0, 1, 5, 42, 1337]:
                for file_id_num in range(9):
                    i_3 = utils.fragement_id_to_i_3(i)
                    file_id = f"{utils.int_to_base3_str(file_id_num):0>2}"
                    parity = utils.compute_checksum(i_3, file_id)
                    self.assertTupleEqual((string, i_3, file_id, parity),
                                          steps.reverse_step_9(
                                              steps.step_9(string, i_3,
                                                                 file_id,
                                                                 parity))
                                          )

    def test_length_of_step_10(self):
        for string in self.test_strings_fragments:
            fragment = string + "ATGCG" * 3
            for number in range(4):
                self.assertEqual(117, len(
                    steps.step_10(fragment, number)))

    def test_reversion_of_step_10(self):
        for string in self.test_strings_fragments:
            fragment = string + "ATGCG" * 3
            for number in range(4):
                self.assertEqual(fragment, steps.reverse_step_10(
                    steps.step_10(fragment, number)))

    def test_reversion_of_step_10_with_compl_reverse(self):
        for string in self.test_strings_fragments:
            fragment = string + "ATGCG" * 3
            for number in range(4):
                self.assertEqual(fragment, steps.reverse_step_10(
                    utils.complement_nucleotides(
                        steps.step_10(fragment, number))[::-1]))


if __name__ == '__main__':
    unittest.main()
