test:
	python3 -m unittest -v test/test_*.py
	pylint goldman_dna

.DEFAULT_GOAL := build

build:
	python3 setup.py sdist bdist_wheel

.PHONY: test build
