#!/bin/env python3

"""
Util to parse the huffman code table
"""

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Util to parse the huffman code table")
    parser.add_argument("file", help="The table to parse")
    args = parser.parse_args()

    huffman_code = []

    # read as binary since encodings get confused from the special bytes in the file
    with open(args.file, 'rb') as file:
        for line in file.readlines():
            line_split = line.split(b'\t')
            byte_number = line_split[2].decode("ascii")
            trits = line_split[3].decode("ascii")
            if byte_number.isdigit():
                huffman_code.append((int(byte_number), trits))

    byte_to_trits = dict(huffman_code)
    trits_to_byte = {trits: byte_number for byte_number, trits in huffman_code}

    print(f"Read {len(huffman_code)} entries")
    print(byte_to_trits)
    print()
    print(trits_to_byte)
