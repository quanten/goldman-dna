#!/bin/env python3

"""
A simple script to remove the annoying slashes from the zeros in the paper
"""

if __name__ == '__main__':
    string = input().encode("utf-8")
    string_clean = bytearray()
    for i in string:
        if i <= 128:
            string_clean.append(i)
    print(string_clean.decode("utf-8"))