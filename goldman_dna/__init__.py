#!/bin/env python3

"""
Implementation of the Goldman et al (2013) algorithm in version 2.0
"""

import random
from typing import Callable

from goldman_dna import steps


def encode(data: bytes, file_id_num: int = 0,
           random_numbers: Callable[[], int] = None) -> list:
    """
    Encodes binary data into a list of DNA strings, ready for synthesis

    :param data: the data to encode as bytes
    :param file_id_num: a file id, 0<=file_id_num<=8
    :param random_numbers: a Callable with no arguments that returns int
                           enables reproducible testing.
                           In production, omit or set to None,
                                than random.randrange(4) will be used
    :return: a list of str, each with a length of 117 and consisting of A,T,G,C
    """
    if random_numbers is None:
        random_numbers = lambda: random.randrange(4)
    if not 0 <= file_id_num <= 8:
        ValueError("file id must be >= 0 and <= 8!")
    s_1 = steps.step_2(data)
    s_4 = steps.step_3(s_1)
    s_5 = steps.step_4(s_4)
    f_i = steps.step_5_6(s_5)
    for i, value in enumerate(f_i):
        f_i[i] = steps.step_7(value, i)
        i_3, file_id, parity = steps.step_8(i, file_id_num)
        f_i[i] = steps.step_9(f_i[i], i_3, file_id, parity)
        f_i[i] = steps.step_10(f_i[i], random_numbers())
    return f_i


def decode(strands: list, logger: Callable[[str], None] = None) -> dict:
    # pylint: disable=too-many-locals
    """
    Decodes a list of strands into binary data

    :param strands: a list of strands
    :param logger: a Callable[[str], None] for information during decoding
                    omit or None for no information
    :return: a dict, file_id as key, binary data or None as value. None if
              the file_id was observed but the file could not be decoded
    """
    # make logger callable
    if logger is None:
        logger = lambda x: None

    # remove duplicates
    unique_strands = list(set(strands))
    logger(f"got {len(unique_strands)} unique strands")

    # check for length, remove duplicates if needed, reverse step 10
    correct_strands = []
    for strand in map(lambda x: x.strip(), unique_strands):
        if len(strand) == 117:
            correct_strands.append(steps.reverse_step_10(strand))
        elif len(strand) > 117:
            cleaned_strand = _remove_duplicates(strand)
            if len(cleaned_strand) == 117:
                correct_strands.append(steps.reverse_step_10(cleaned_strand))
        # all shorter strands are not usable
    logger(f"got {len(correct_strands)} strands after length check")

    # read index information, check parity, separate by file_id
    # reverse step 9, 8, 7
    strands_per_fileid = {}
    for strand in correct_strands:
        try:
            f_i, i_3, file_id, parity = steps.reverse_step_9(strand)
            strand_index, file_id_num = steps.reverse_step_8(i_3, file_id, parity)
            strands_of_file = strands_per_fileid.get(file_id_num, dict())
            strands_of_file[strand_index] = steps.reverse_step_7(f_i, strand_index)
            strands_per_fileid[file_id_num] = strands_of_file
        except ValueError:
            # if there was a ValueError, the strand cannot be used
            pass
    logger(f"got file_id's {', '.join(map(str, strands_per_fileid.keys()))}")

    # reassemble files
    files = {}
    for current_file_id in strands_per_fileid:
        s_5 = steps.reassemble_step_5_6(strands_per_fileid[current_file_id])
        if len(s_5) == 0:
            logger(f"reassembly of file {current_file_id} out of "
                   f"{len(strands_per_fileid[current_file_id])} failed")
            files[current_file_id] = None
            continue

        try:
            s_4 = steps.reverse_step_4(s_5)
            s_1 = steps.reverse_step_3(s_4)
            data = steps.reverse_step_2(s_1)
            files[current_file_id] = data
            logger(f"reassembly of file {current_file_id} was successful")
        except ValueError as error:
            logger(f"reassembly of file {current_file_id} was possible"
                   f"but then error {error} occurred")
            files[current_file_id] = None

    return files


def _remove_duplicates(strand: str) -> str:
    """
    Remove successive duplicates

    :param strand: a string
    :return: the string with no successive duplicates
    """
    clean_strand = strand[0]
    for i in range(1, len(strand)):
        if strand[i - 1] != strand[i]:
            clean_strand += strand[i]
    return clean_strand
