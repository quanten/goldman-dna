#!/bin/env python3

"""
Implementation of the keystream method, described in Step 1.7
"""

KEYSTREAMS = [
    "002000010110102111112122210011122221010102121222022000221201020221"
    "002121121000212222021211121122221",
    "202020122121210120001200210222112020222022222220220001221012111022"
    "121120202022211221112202002121022",
    "221221101200221120220011002222100000020200021121021020122100021201"
    "010210202002000101020022121100100",
    "100122100011112100120210020011102201122122100100120122212000021220"
    "022012202201100010212222110222020"
]

NUCLEOTIDE_TO_NUMERIC = {
    "A": 0, "C": 1, "G": 2, "T": 3
}

NUMERIC_TO_NUCLEOTIDE = {
    0: "A", 1: "C", 2: "G", 3: "T"
}


def apply_keystream(strand: str, keystream: str, substract: bool) -> str:
    """
    Apply the keystream to the strand of nucleotides

    For step 1.7

    :param strand: a string of nucleotides
    :param keystream: a string of trits (0, 1, 2)
    :param substract: if True, keystream will be subtracted, otherwise added
    :return: a string of nucleotides
    """
    if len(strand) != (len(keystream) + 1):
        raise ValueError("strand must be one char longer than keystream!")
    # convert nucleotides to numeric values 0, 1, 2, 3
    numeric_strand = list(map(lambda x: NUCLEOTIDE_TO_NUMERIC[x], strand))
    # convert keystream into numeric values and use factor to subtract if wanted
    factor = 1 if not substract else -1
    numeric_keystream = list(map(lambda x: int(x) * factor, keystream))

    # calculate differences and apply keystream
    differences = []
    for i in range(1, len(numeric_strand)):
        diff = (numeric_strand[i] - numeric_strand[i - 1]) % 4
        diff = (diff - 1) % 3
        diff = (diff + numeric_keystream[i - 1]) % 3
        differences.append((diff + 1) % 4)

    # reconstruct strand through adding
    numeric_encrypted = [numeric_strand[0]]
    for diff in differences:
        numeric_encrypted.append((diff + numeric_encrypted[-1]) % 4)

    # convert numeric strand back to nucleotides
    return "".join(map(lambda x: NUMERIC_TO_NUCLEOTIDE[x], numeric_encrypted))


def encrypt(strand: str, index: int) -> str:
    """
    Encrypt a nucleotide string with keystream matching the index

    Step 1.7

    :param strand: a nucleotide string
    :param index: the index
    :return: the encrypted nucleotide string
    """
    return apply_keystream(strand, KEYSTREAMS[index % 4], False)


def decrypt(strand: str, index: int) -> str:
    """
    Decrypt a nucleotide string with keystream matching the index

    Reversion of step 1.7

    :param strand: a nucleotide string
    :param index: the index
    :return: the decrypted nucleotide string
    """
    return apply_keystream(strand, KEYSTREAMS[index % 4], True)
