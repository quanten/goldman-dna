#!/bin/env python3

"""
A command line interface for this package
"""

import argparse
import logging

import goldman_dna
from goldman_dna import compression
from goldman_dna import steps

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="A implementation of the "
                                                 "method from Goldman et. al "
                                                 "(2013) to encode arbitrary "
                                                 "files into DNA fragments.")
    parser.add_argument("file", type=str, nargs="+",
                        help="the file to encode/decode")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print more information")
    parser.add_argument("-l", "--long-strand", action="store_true",
                        help="don't split strand, only perform step 2-4. "
                             "many options won't work in this mode")
    parser.add_argument("-c", "--compression", action="store_true",
                        help="use compression (for encoding / decoding)")
    en_decode_group = parser.add_mutually_exclusive_group(required=True)
    en_decode_group.add_argument("-e", "--encode", action="store_true",
                                 help="encode file")
    en_decode_group.add_argument("-d", "--decode", action="store_true",
                                 help="decode file")
    multi_file_group = parser.add_mutually_exclusive_group()
    multi_file_group.add_argument("-m", "--multi-file", action="store",
                                  help="prefix for output file.\n"
                                       "encodes up to 9 files into one DNA file"
                                       ", distinguished by file id / decode all"
                                       " files in DNA file")
    multi_file_group.add_argument("-i", "--id", action="store", type=int,
                                  default=0,
                                  help="encode with this file id / decode file "
                                       "with this file id, default 0")
    args = parser.parse_args()
    logger = logging.debug

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if args.long_strand:
        if args.multi_file is not None or args.id != 0 or args.compression:
            parser.error(
                "in long-strand mode, only encode, decode and verbose are "
                "available!")
        for file_name in args.file:
            if args.encode:
                with open(file_name, "rb") as input_file:
                    s_5 = steps.step_4(
                        steps.step_3(steps.step_2(input_file.read())))
                    with open(file_name + ".dna", "w") as output_file:
                        output_file.write(s_5)
            else:
                with open(file_name, "r") as input_file:
                    s_5 = input_file.read().strip()
                    data = steps.reverse_step_2(
                        steps.reverse_step_3(steps.reverse_step_4(s_5)))
                    with open(file_name + ".decode", "wb") as output_file:
                        output_file.write(data)
    else:
        if args.encode:
            if args.multi_file is not None:
                if len(args.file) > 9:
                    parser.error("with multi-file, only up top 9 files are possible!")
                dna_strands = []
                for i, file_name in enumerate(args.file):
                    logger(f"encode {file_name} with file id {i}")
                    with open(file_name, "rb") as input_file:
                        dna_strand = goldman_dna.encode(input_file.read(),
                                                        file_id_num=i)
                        logger(f"encoded {file_name} into {len(dna_strand)} "
                               f"dna strands")
                        dna_strands += dna_strand
                logger(f"encoded a total of {len(dna_strands)} dna strands")
                if args.compression:
                    with open(args.multi_file + ".dna.comp",
                              "wb") as output_file:
                        output_file.write(
                            compression.compress("".join(dna_strands)))
                else:
                    with open(args.multi_file + ".dna", "w") as output_file:
                        output_file.write("\n".join(dna_strands))
            else:
                for i, file_name in enumerate(args.file):
                    with open(file_name, "rb") as input_file:
                        dna_strands = goldman_dna.encode(input_file.read(),
                                                         file_id_num=args.id)
                        logger(
                            f"encoded {file_name} into {len(dna_strands)} "
                            f"strands")
                        if args.compression:
                            with open(file_name + ".dna.comp",
                                      "wb") as output_file:
                                output_file.write(
                                    compression.compress("".join(dna_strands)))
                        else:
                            with open(file_name + ".dna", "w") as output_file:
                                output_file.write("\n".join(dna_strands))
        else:
            if args.multi_file is not None:
                dna_strands = []
                for file_name in args.file:
                    if args.compression:
                        with open(file_name, "rb") as input_file:
                            dna_strand = compression.decompress(
                                input_file.read())
                            dna_strands += [(dna_strand[offset:offset + 117])
                                            for
                                            offset in
                                            range(0, len(dna_strand), 117)]
                    else:
                        with open(file_name, "r") as input_file:
                            dna_strands += input_file.readlines()
                decoded = goldman_dna.decode(dna_strands, logger=logger)
                for file_id in decoded:
                    if decoded[file_id] is not None:
                        file_name = args.multi_file + f".decoded.{file_id}"
                        print(f"found file id {file_id}, wrote {file_name}")
                        with open(file_name, "wb") as output_file:
                            output_file.write(decoded[file_id])
                    else:
                        print(f"found file id {file_id} but file could not be "
                              f"reassembled")
            else:
                for file_name in args.file:
                    if args.compression:
                        with open(file_name, "rb") as input_file:
                            dna_strand = compression.decompress(
                                input_file.read())
                            dna_strands = [(dna_strand[offset:offset + 117]) for
                                           offset in
                                           range(0, len(dna_strand), 117)]
                    else:
                        with open(file_name, "r") as input_file:
                            dna_strands = input_file.readlines()
                    decoded = goldman_dna.decode(dna_strands, logger=logger)
                    if args.id in decoded:
                        if decoded[args.id] is not None:
                            with open(file_name + ".decoded",
                                      "wb") as output_file:
                                output_file.write(decoded[args.id])
                        else:
                            print(
                                f"file id {args.id} was found, but file could "
                                f"not be reassembled!")
                    else:
                        print(f"file id {args.id} was not found in DNA data!")
