#!/bin/env python3

"""
This is a compression algorithm to specifically to compress a set of DNA strands
out of the algorithm of Goldmand et al (2013)

This "compression algorithm" is just a dictionary coding and does not remove any
other redundancies. Because, due to the keystreams in step 7, we intentionally
introduce more entropy, a entropy coding wouldn't be much better.

It encodes five trits (0,1,2) (since no repeating nucleotides are allowed, the
strand can be represented as trits) into one byte
3^5 = 243 into 2^8 = 256 -> 95% efficiency

limitations:
- length must be a multiple of 117
- no repeating nucleotides are allowed
- strand must not begin with "G"
"""

from goldman_dna import utils


def compress(strand: str) -> bytes:
    """
    Compress a DNA strand with a dictionary coding into binary data

    :param strand: the strand as str, must not begin with "G"!
    :return: a bytes
    """
    trits = utils.nucleotides_to_trits(strand, "G")
    trits += "0" * ((5 - (len(trits) % 5)) % 5)
    compressed_data = bytearray()
    for index in range(len(trits) // 5):
        compressed_data.append(utils.base3_str_to_int(trits[5 * index: 5 * index + 5]))
    return bytes(compressed_data)


def decompress(compressed_data: bytes) -> str:
    """
    Decompress a DNA strand which was compressed with compression.compress

    The output will be truncated to a multiple of 117

    :param compressed_data: the compressed data as bytes
    :return: the DNA strand as str
    """
    trits = ""
    for byte in compressed_data:
        trits += f"{utils.int_to_base3_str(byte):0>5}"
    strand = utils.trits_to_nulceotides(trits, "G")
    return strand[:len(strand) - (len(strand) % 117)]
