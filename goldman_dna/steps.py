#!/bin/env python3

"""
All steps of the method
"""

from goldman_dna import huffman, utils, keystream


def step_2(data: bytes) -> str:
    """
    Converts a bytes into a string of trits, Step 1.2

    :param data: data as bytes
    :return: a string of trits (0, 1 or 2)
    """
    return huffman.encode(data)


def reverse_step_2(trits: str) -> bytes:
    """
    Converts a string of trits into a bytes, reversion of Step 1.2

    :param trits: a string of trits (0, 1 or 2)
    :return: a bytes
    """
    return huffman.decode(trits)


def step_3(s_1: str) -> str:
    """
    Calculates the lengths, prepend it and pad to 25 trits

    :param s_1: the data trit sequence
    :return: s_4
    """
    s_1_n = len(s_1)
    # length as base3 with front padding
    s_2 = f"{utils.int_to_base3_str(s_1_n):0>25}"
    # calc padding for the end
    s_3 = "0" * ((25 - ((len(s_1) + len(s_2)) % 25)) % 25)
    s_4 = s_2 + s_1 + s_3
    return s_4


def reverse_step_3(s_4: str) -> str:
    """
    Strip data trit sequence from length information and padding

    Also checks the length and raise a ValueError if corrupt
    Also raises a ValueError if the padding is not zeros

    :param s_4: s_4, the trit sequence with length and padding
                    length must be a multiple of 25
    :return: s_1, the extracted data trit sequence
    """
    if len(s_4) % 25 != 0:
        raise ValueError("Length of s_4 must be a multiple of 25!")
    # get the length parameter
    s_2 = s_4[:25]
    s_1_n = utils.base3_str_to_int(s_2)
    if not s_1_n <= len(s_4) - 25 <= s_1_n + 25:
        raise ValueError("Length information is corrupt!")
    s_3 = s_4[s_1_n + 25:]
    if s_3 != "0" * len(s_3):
        raise ValueError("Padding is not zeros!")
    # cut the data, discard 25 trits length at front and the padding at the end
    s_1 = s_4[25:s_1_n + 25]
    return s_1


def step_4(s_4: str) -> str:
    """
    Convert trits to a nucleotide string using trits_to_nulceotides()

    :param s_4: a string containing 0, 1 or 2
    :return: a representation with four nucleotides without repetition
    """
    return utils.trits_to_nulceotides(s_4, "A")


def reverse_step_4(s_5: str) -> str:
    """
    Convert a nucleotide string to trits using nucleotides_to_trits()

    :param s_5: the sequence of nucleotides
    :return: a string of trits
    """
    return utils.nucleotides_to_trits(s_5, "A")


def step_5_6(s_5: str) -> list:
    """
    Split a nucleotide string into overlapping pieces
    and reverse complement the odd pieces

    :param s_5: a string of nucleotides, length must be a multiple of 25
    :return: a list of nucleotide strings, each with a length of 100
    """
    length_s_5 = len(s_5)
    if length_s_5 % 25 != 0:
        raise ValueError("Length of s_5 must be a multiple of 25!")
    f_i = []
    for i in range(0, length_s_5 // 25 - 3):
        f_i.append(s_5[25 * i:25 * i + 100])
        # if i is odd, than save the reverse complement
        if i % 2 == 1:
            f_i[i] = utils.complement_nucleotides(f_i[i])[::-1]
    return f_i


def reverse_step_5_6(f_i: list) -> str:
    """
    Assemble overlapping pieces back together, after reverse complementing the
    odd ones

    The overlapping parts must be identical,
    the strands must be in correct order without some missing,
    otherwise a ValueError is raised,
    here is no error correction or error handling!
    See reassemble_step_5_6 for a version which does reordering
    and error correction

    :param f_i: a list of nucleotide strings, each with a length of 100
    :return: the assembled string of nucleotides
    """
    # check for correct length of every strand
    for strand in f_i:
        if len(strand) != 100:
            raise ValueError("Each segment must have length 100!")
    # start with the first
    s_5 = f_i[0]
    # iterate through the rest
    for i in range(1, len(f_i)):
        # reverse and complement if odd
        if i % 2 == 1:
            strand = utils.complement_nucleotides(f_i[i])[::-1]
        else:
            strand = f_i[i]
        # check if overlap is identical
        if strand[:75] != s_5[-75:]:
            raise ValueError("Overlapping parts must be identical!")
        # add the last 25 nucleotides, since the rest is overlapping
        s_5 += strand[-25:]
    return s_5


def reassemble_step_5_6(strands: dict) -> str:
    """
    Assemble strands back together, after reverse complementing the odd ones

    This function does error correction with the majority rule in the
    overlapping parts

    :param strands: a dict, strand index as key, strand as str as value
    :return: the reassembled s_5 or a empty str if reassembly was not successful
    """
    # remove strands with wrong length
    for index in strands:
        if len(strands[index]) != 100:
            del strands[index]
        elif index % 2 == 1:
            strands[index] = utils.complement_nucleotides(strands[index])[::-1]

    # a fragment is a quarter of a strand, 25 in length
    # create fragment space
    fragments = [None] * (max(strands.keys()) + 3 + 1)

    # check all possible fragments
    for fragment_index in range((max(strands.keys()) + 3 + 1)):
        fragment_candidates = dict()
        # check in all four possible strands
        for offset in range(4):
            if fragment_index - offset in strands:
                fragment = strands[fragment_index - offset] \
                    [25 * offset: 25 * offset + 25]
                fragment_candidates[fragment] = fragment_candidates.get(
                    fragment, list()) + [fragment_index - offset]
        # find the fragment which is in the most strands
        ranked_fragments = sorted(fragment_candidates.keys(),
                                  key=lambda frag,
                                             fc=fragment_candidates:
                                  len(fc[frag]),
                                  reverse=True)
        if len(ranked_fragments) == 1:
            # there was only one candidate, so take it
            fragments[fragment_index] = ranked_fragments[0]
        elif len(ranked_fragments) > 1:
            if len(fragment_candidates[ranked_fragments[0]]) > len(
                    fragment_candidates[ranked_fragments[1]]):
                # there is a clear vote
                fragments[fragment_index] = ranked_fragments[0]

    if None in fragments:
        # there are missing parts
        return ""
    # all found :) returning
    return "".join(fragments)


def step_7(strand: str, index: int) -> str:
    """
    Encrypt a nucleotide string with keystream matching the index

    :param strand: a nucleotide string
    :param index: the index
    :return: the encrypted nucleotide string
    """
    return keystream.encrypt(strand, index)


def reverse_step_7(strand: str, index: int) -> str:
    """
    Decrypt a nucleotide string with keystream matching the index

    :param strand: a nucleotide string
    :param index: the index
    :return: the decrypted nucleotide string
    """
    return keystream.decrypt(strand, index)


def step_8(index: int, file_id_num: int) -> tuple:
    """
    Converts index information into trits and compute checksum

    :param index: the fragment index
    :param file_id_num: the file_id as int
    :return: (i_3, file_id, parity), all as string of trits
    """
    i_3 = utils.fragement_id_to_i_3(index)
    file_id = f"{utils.int_to_base3_str(file_id_num):0>2}"
    parity = utils.compute_checksum(i_3, file_id)
    return i_3, file_id, parity


def reverse_step_8(i_3: str, file_id: str, parity: str = None) -> tuple:
    """
    Converts the index information back into integer

    :param i_3: the fragment index as trits, length 12
    :param file_id: the file_id as trits, length 2
    :param parity: optional, a parity. Raises ValueError on mismatch if given
    :return: (index, file_id_num), both as int
    """
    if len(i_3) != 12:
        raise ValueError("i_3 must have the length 12")
    if len(file_id) != 2:
        raise ValueError("file_id must have the length 2")
    if parity is not None and parity != utils.compute_checksum(i_3, file_id):
        raise ValueError("Parity Error!")
    return utils.base3_str_to_int(i_3), utils.base3_str_to_int(file_id)


def step_9(f_i: str, i_3: str, file_id: str, parity: str) -> str:
    """
    Append indexing information and checksum

    :param f_i: the nucleotide string, length 100
    :param i_3: the string index, encoded as trits and padded to 12
    :param file_id: the file id, encoded as trits, 2 long
    :param parity: the checksum, encoded as a single trit
    :return: f_i_tick, the nucleotide string with indexing information
    """
    if len(f_i) != 100:
        raise ValueError("f_i must have the length 100!")
    if len(i_3) != 12:
        raise ValueError("i_3 must have the length 12!")
    if len(file_id) != 2:
        raise ValueError("file_id must have the length 2!")
    if len(parity) != 1:
        raise ValueError("parity must be a single char!")

    ix_trits = file_id + i_3 + parity
    ix_nulceotides = utils.trits_to_nulceotides(ix_trits, f_i[-1])
    return f_i + ix_nulceotides


def reverse_step_9(f_i_tick: str) -> tuple:
    """
    Read and strip indexing information
    Currently this function raises a ValueError if there is a party mismatch,
    this may change in future

    :param f_i_tick: the nucleotide string with indexing information
    :return: a tuple: f_i, i_3, file_id, parity
    """
    if len(f_i_tick) != 115:
        ValueError("f_i_tick must have the length 115 (100 data, 15 index)!")
    parity = utils.nucleotides_to_trits(f_i_tick[-1], f_i_tick[-2])
    i_3 = utils.nucleotides_to_trits(f_i_tick[-13:-1], f_i_tick[-14])
    file_id = utils.nucleotides_to_trits(f_i_tick[-15:-13], f_i_tick[-16])
    if parity != utils.compute_checksum(i_3, file_id):
        raise ValueError("Parity Error!")
    return f_i_tick[:100], i_3, file_id, parity


def step_10(f_i_tick: str, random_number: int) -> str:
    """
    Prepends and appends a nucleotide for orientation

    :param f_i_tick: a nucleotide string with indexing information, length 115
    :param random_number: a random integer mod 4, enables reproducible testing
                          use random.randrange(4) in production
    :return: f_i_tick_tick, length 117
    """
    if len(f_i_tick) != 115:
        ValueError("f_i_tick must have the length 115 (100 data, 15 index)!")
    prepending_nucleotide = ("A", "T")[random_number % 2]
    if prepending_nucleotide == f_i_tick[0]:
        prepending_nucleotide = utils.NUCLEOTIDE_COMPLEMENT[
            prepending_nucleotide]
    appending_nucleotide = ("G", "C")[(random_number // 2) % 2]
    if appending_nucleotide == f_i_tick[-1]:
        appending_nucleotide = utils.NUCLEOTIDE_COMPLEMENT[appending_nucleotide]
    return prepending_nucleotide + f_i_tick + appending_nucleotide


def reverse_step_10(f_i_tick_tick: str) -> str:
    """
    Strip orientation nucleotides and reverse complement if needed

    :param f_i_tick_tick: a nucleotide string, length 117
    :return: f_i_tick, length 115
    """
    if len(f_i_tick_tick) != 117:
        ValueError("f_i_tick_tic must have the length 117!")
    if f_i_tick_tick[0] in ["G", "C"]:
        f_i_tick_tick = utils.complement_nucleotides(f_i_tick_tick)[::-1]
    return f_i_tick_tick[1:-1]
