#!/bin/env python3

"""
Some useful functions and constants used in the package
"""

TRIT_TO_NUCLEOTIDE = {
    "A": {0: "C", 1: "G", 2: "T"},
    "C": {0: "G", 1: "T", 2: "A"},
    "G": {0: "T", 1: "A", 2: "C"},
    "T": {0: "A", 1: "C", 2: "G"},
}

NUCLEOTIDE_TO_TRIT = {
    "A": {"C": 0, "G": 1, "T": 2},
    "C": {"G": 0, "T": 1, "A": 2},
    "G": {"T": 0, "A": 1, "C": 2},
    "T": {"A": 0, "C": 1, "G": 2},
}

NUCLEOTIDE_COMPLEMENT = {
    "A": "T", "T": "A", "G": "C", "C": "G"
}


def int_to_base3_str(number: int) -> str:
    """
    Converts a integer into a base3 number as string

    :param number: the integer to convert
    :return: the converted string
    """
    string = ""
    while number > 0:
        string = str(number % 3) + string
        number //= 3
    return string if string != "" else "0"


def base3_str_to_int(string: str) -> int:
    """
    Converts a base3 number given as string to a integer

    :param string: base3 number as string
    :return: the converted number
    """
    number = 0
    for char in string:
        number *= 3
        number += int(char)
    return number


def trits_to_nulceotides(trits: str, start_nucleotide: str = "A") -> str:
    """
    Converts a string of trits (0, 1, 2) into a sequence of
    non repeating nucleotides

    :param trits: a string of "0", "1" and "2"
    :param start_nucleotide: the nucleotide used at the starting point
    :return: a string of "A", "C", "G", "T"
    """
    last_nucleotide = start_nucleotide
    result = ""
    for trit in trits:
        last_nucleotide = TRIT_TO_NUCLEOTIDE[last_nucleotide][int(trit)]
        result += last_nucleotide
    return result


def nucleotides_to_trits(nucleotides: str, start_nucleotide: str = "A") -> str:
    """
    Converts a string of nucleotides back to a string of trits (0, 1, 2)

    :param nucleotides: a string of "A", "C", "G", "T"
    :param start_nucleotide: the nucleotide used at the starting point
    :return: a string of "0", "1" and "2"
    """
    last_nucleotide = start_nucleotide
    result = ""
    for nucleotide in nucleotides:
        result += str(NUCLEOTIDE_TO_TRIT[last_nucleotide][nucleotide])
        last_nucleotide = nucleotide
    return result


def complement_nucleotides(strand: str) -> str:
    """
    Calculates the complement of a string of nucleotides

    :param strand: a string of nucleotides
    :return: the string with each nucleotide replaced with its complement
    """
    return "".join(map(lambda base: NUCLEOTIDE_COMPLEMENT[base], strand))


def fragement_id_to_i_3(index: int) -> str:
    """
    Converts index to base3 and pads it to length 12

    :param index: index of the fragment
    :return: i_3, the converted and padded index
    """
    return f"{int_to_base3_str(index):0>12}"


def compute_checksum(i_3: str, file_id: str) -> str:
    """
    Compute the checksum P as described in step 1.8

    :param i_3: i_3, the string index, encoded as trits and padded to 12
    :param file_id: the file ID as trits
    :return: a single trit ("0", "1", "2")
    """
    if len(i_3) != 12:
        raise ValueError("i_3 must have the length 12!")
    if len(file_id) != 2:
        raise ValueError("file_id must have the length 2!")
    checksum = int(file_id[0])
    for i in range(0, 12, 2):
        checksum += int(i_3[i])
    return str(checksum % 3)
