#/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="goldman-dna-quanten",
    version="2.0",
    author="quanten",
    license="MIT",
    description="Encode data in DNA",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/quanten/goldman-dna",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: System :: Archiving :: Compression",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ],
    python_requires='>=3.6'
)